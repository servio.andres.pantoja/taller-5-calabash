Feature: Testeando funcionalidad de puntos de atención

  Scenario: Remover la pantalla de bienvenida inicial
    Given I press "Rutas"

  Scenario: Ver todos los puntos de atención
    Given I wait to see "Inicio"
    And I click on screen 80% from the left and 90% from the top
    When I press "TODOS"
    Then I should see ".Naranj@"

  Scenario: Ver los puntos de recarga
    Given I wait to see "Inicio"
    And I click on screen 80% from the left and 90% from the top
    When I press "RECARGA"
    Then I should see ".Naranj@"

  Scenario: Ver los puntos de personalización
    Given I wait to see "Inicio"
    And I click on screen 80% from the left and 90% from the top
    When I press "PERSONALIZACIÓN"
    Then I should see "Cade Candelaria"

  Scenario: Buscar un punto de recarga
    Given I wait to see "Inicio"
    And I click on screen 80% from the left and 90% from the top
    When I press "TODOS"
    And I click on screen 70% from the left and 10% from the top
    Then I enter text "Pagatodo" into field with id "search_src_text"
    And I press the enter button
    Then I should see "Pagatodo"