Feature: Testeando funcionalidad de rutas

    Scenario: Remover la pantalla de bienvenida inicial
        Given I press "Rutas"

    Scenario: Como usuario deseo ver las rutas que provee transmilenio
        Given I press "Rutas"
        When I press "TRANSMILENIO"
        Then I should see "Portal El Dorado"

    Scenario: Como usuario deseo ver las rutas que provee un bus urbano
        Given I press "Rutas"
        When I press "URBANO"
        Then I should see "San fernando"

    Scenario: Como usuario deseo ver las rutas que provee un bus complementario
        Given I press "Rutas"
        When I press "COMPLEMENTARIO"
        Then I should see "Guaymaral"

    Scenario: Como usuario deseo ver las rutas que provee un bus especial
        Given I press "Rutas"
        When I press "COMPLEMENTARIO"
        And I press "ESPECIAL"
        Then I should see "Sidel"

    Scenario: Como usuario deseo ver las rutas que provee un bus alimentador
        Given I press "Rutas"
        When I press "COMPLEMENTARIO"
        And I press "ESPECIAL"
        And I press "ALIMENTADOR"
        Then I should see "Álamos"

    Scenario: Como usuario deseo ver las estaciones de una ruta de transmilenio
        Given I press "Rutas"
        When I press "1"
        Then I should see "Cll. 26"